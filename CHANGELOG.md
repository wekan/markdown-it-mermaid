# 0.5.3
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.14.0
-   Update [@wekanteam/cli-table3](https://github.com/wekan/cli-table3/) 0.7.3
# 0.5.2
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.13.8
-   Update [@wekanteam/cli-table3](https://github.com/wekan/cli-table3/) 0.7.2
# 0.5.1
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.13.6
-   Update [@wekanteam/cli-table3](https://github.com/wekan/cli-table3/) 0.7.1
-   Deleted devDependencies.
# 0.5.0
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.13.3
-   Update [@wekanteam/cli-table3](https://github.com/wekan/cli-table3/) 0.7.0
# 0.4.4
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.13.0
# 0.4.3
-   Update [Mermaid-js](https://mermaid-js.github.io/mermaid/#/) 8.10.2
-   [Add "dictionary" option](https://github.com/liradb2000/markdown-it-mermaid#customize-mermaid)
    -   Customizable 'token' ( default: 'mermaid' )
    -   Replacable keywords like 'graph', 'sequenceDiagram'
    -   and more...
# 0.4.2
-   Support Mermaid 8.8.4 New Diagrams (https://mermaid-js.github.io/mermaid/#/)
-   Remove the security issues

# 0.4.1
-   New diagrams (classDiagram)
-   Support All grpahs (Thx @DatatracCorporation, @nojaja)
-   Support securityLevel options
-   Use stable id for mermaid(@DatatracCorporation)
